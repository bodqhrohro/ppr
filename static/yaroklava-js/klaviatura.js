/*
Яроклава, (С) Денис Будяк budden73@mail.ru 2018
На основе Instant Translit, Created by Dima Kuprijanov, http://dimonchik.com/
*/


function str_replace ( search, replace, subject ) {

    alert("str_replace не проверялась, попробуй https://stackoverflow.com/questions/1144783/how-to-replace-all-occurrences-of-a-string-in-javascript!")
    if(!(replace instanceof Array)){
        replace=new Array(replace);
        if(search instanceof Array){
            while(search.length>replace.length){
                replace[replace.length]=replace[0];
            }
        }
    }

    if(!(search instanceof Array))search=new Array(search);
    while(search.length>replace.length){
        replace[replace.length]='';
    }

    if(subject instanceof Array){
        for(k in subject){
            subject[k]=str_replace(search,replace,subject[k]);
        }
        return subject;
    }

    for(var k=0; k<search.length; k++){
        var i = subject.indexOf(search[k]);
        while(i>-1){
            subject = subject.replace(search[k], replace[k]);
            i = subject.indexOf(search[k],i);
        }
    }

    return subject;

}

var ГенерируйКодДляAutoHotkey;
var ГенерируйКодДляФайлаSymbols; 

$(document).ready(function ($) {

    $.fn.ё = function () {
        if (!(this instanceof Object)) {
            б = new Error("ё - не выборка");
            console.log(б.stack);
            throw б;
        }
        if (this.length === 0) {
            б = new Error("ё: пустая выборка");
            console.log(б.stack);
            throw б;
        }
        return this;
    }


    var КодКлавиши = {};
    var кодТекущейРаскладки = "ru";
    const КодКлавишиПробел = 32;
    const КодКлавишиShift = 16;

    function УстановиОбработчикиКлавиатуры(one_textarea) {
        one_textarea.keydown(function (e) {
            $(this).Instant_translit({event_now: e, obect_now: this});
        });
        one_textarea.keyup(function (e) {
            $(this).Instant_translit({event_now: e, obect_now: this});
        });
    };

    function УгадайБраузер() {
        const uA = navigator.userAgent;
        if (/Chrome\//.test(uA)) {
            return "Chrome";
        } else if (/.NET\ /.test(uA)) {
            return "IE";
        } else if (/Firefox\//.test(uA)) {
            return "Firefox";
        } else if (/Safari\/6/.test(uA)) {
            return "Safari/6**";
        }
        return "Unsupported";
    }

    const Браузер = УгадайБраузер();
    const FirefoxP = (Браузер === "Firefox");

    $("#СтрСостПримечание").text("Ваш браузер похож на " + Браузер + " (" + navigator.userAgent + ")");

    if (Браузер === "Unsupported") {
        alert("Ваш браузер не поддерживается");
        return;
    }

    // формат примерно сопоставим с greywyvern_keyboard. Элементы - обычный, Shift, Пробел, Shift-Пробел. 
    // Если отсутствует в таблице,
    // то ALERT
    
    const Раскладка2 = [
            [["ё", "Ё", "`", "~"], 
             ["1", "!", "☼", "≠"], 
             ["2", '"', "@", "@"], 
             ["3", "№", "#", "§"], 
             ["4", ";", "$", "†"], 
             ["5", "%", "°", "<НЕТ>"], 
             ["6", ":", "^", "☭"], 
             ["7", "?", "&", "¿"], 
             ["8", "*", "<НЕТ>", "<НЕТ>"], 
             ["9", "(", "<НЕТ>", "<НЕТ>"], 
             ["0", ")", "<НЕТ>", "<НЕТ>"], 
             ["-", "_", "<НЕТ>", "<НЕТ>"], 
             ["=", "+", "≈", "≡"]
             /*, ["Bksp", "Bksp"]*/]
            , [/*["Tab", "Tab"],*/ 
             ["й", "Й", "q", "Q"], 
             ["ц", "Ц", "w", "W"], 
             ["у", "У", "e", "E"], 
             ["к", "К", "r", "R"], 
             ["е", "Е", "t", "T"], 
             ["н", "Н", "y", "Y"], 
             ["г", "Г", "u", "U"], 
             ["ш", "Ш", "i", "I"], 
             ["щ", "Щ", "o", "O"], 
             ["з", "З", "p", "P"], 
             ["х", "Х", "[", "{"], 
             ["ъ", "Ъ", "]", "}"], 
             ["\\", "♥", "|", "<НЕТ>"]]
            , [/*["Caps", "Caps"],*/
             ["ф", "Ф", "a", "A"], 
             ["ы", "Ы", "s", "S"], 
             ["в", "В", "d", "D"], 
             ["а", "А", "f", "F"], 
             ["п", "П", "g", "G"], 
             ["р", "Р", "h", "H"], 
             ["о", "О", "j", "J"], 
             ["л", "Л", "k", "K"], 
             ["д", "Д", "l", "L"], 
             ["ж", "Ж", "<НЕТ>", "<НЕТ>"], 
             ["э", "Э", "'", "<НЕТ>"]
             /*,["Enter", "Enter"]*/]
            , [/*["Shift", "Shift"]*/
             ["я", "Я", "z", "Z"], 
             ["ч", "Ч", "x", "X"], 
             ["с", "С", "c", "C"], 
             ["м", "М", "v", "V"], 
             ["и", "И", "b", "B"], 
             ["т", "Т", "n", "N"], 
             ["ь", "Ь", "m", "M"], 
             ["б", "Б", "<", "«"], 
             ["ю", "Ю", ">", "»"], 
             [".", ",", " ", "/"]
             /*["Shift", "Shift"]*/]
            ,[[" ", " ", " ", " "]]
    ];

    // Virtual key codes
    const КодыНаКлавиатуре = [
            [192, 49, 50, 51, 52, 53, 54, 55, 56, 57, 48, (FirefoxP ? 173 : 189), (FirefoxP ? 61 : 187)]
            , [81, 87, 69, 82, 84, 89, 85, 73, 79, 80, 219, 221, 220]
            , [65, 83, 68, 70, 71, 72, 74, 75, 76, (FirefoxP ? 59 : 186), 222]
            , [90, 88, 67, 86, 66, 78, 77, 188, 190, 191]
            , [32]
        ];

    const Значения_key_для_symbols = [
        ["TLDE", "AE01", "AE02", "AE03", "AE04", "AE05", "AE06", "AE07", "AE08", "AE09", "AE10", "AE11", "AE12"],
        ["AD01", "AD02", "AD03", "AD04", "AD05", "AD06", "AD07", "AD08", "AD09", "AD10", "AD11", "AD12", "BKSL"],
        ["AC01", "AC02", "AC03", "AC04", "AC05", "AC06", "AC07", "AC08", "AC09", "AC10", "AC11"],
        ["AB01", "AB02", "AB03", "AB04", "AB05", "AB06", "AB07", "AB08", "AB09", "AB10"],
        ["SPCE"]        
    ];

    /// Изображениям кнопок имеют теги кнотст_Значёк,
    /// где значёк -
    /// - цифра для цифровых кнопок
    /// - кириллица в верхнем регистре для кириллицы
    /// спец знаки: - -> m,
    ///             = -> q,
    ///             \ -> k,
    ///             . -> d
    ///             пробел -> s
    const ТегиИзображенийКнопок = [
        "Ё1234567890mq"
        , "ЙЦУКЕНГШЩЗХЪk"
        , "ФЫВАПРОЛДЖЭ"
        , "ЯЧСМИТЬБЮd"
        , "s"
    ];
    
    // Взято с learn.javascript.ru
    function ГдеЖеЭлементВМассиве(array, value) {
        if (array.indexOf) { // если метод существует
            return array.indexOf(value);
        }

        for (var i = 0; i < array.length; i++) {
            if (array[i] === value) return i;
        }

        return -1;
    }

    function ИзображениеКнопки(Ряд, НомКнопВРяду) {
        const ЗначёкИдаКнопки = ТегиИзображенийКнопок[Ряд].charAt(НомКнопВРяду);
        console.assert(typeof ЗначёкИдаКнопки === "string")
        const ИдЭлементаКнопки = "#рискноп_" + ЗначёкИдаКнопки;
        const Рез = $(ИдЭлементаКнопки);
        console.assert(typeof Рез === "object" && Рез.length === 1);
        return Рез;
    }

    function ПодсветиКнопку(Ряд, НомКнопВРяду) {
        const Изо = ИзображениеКнопки(Ряд, НомКнопВРяду);
        Изо.css('background-color', "blue");
        setTimeout(function () {
            Изо.css('background-color', "");
        }, 50);
    }


    function ПокажиРаскладку(ключРаскладки) {
        const ЧислоРядов = КодыНаКлавиатуре.length;
        for (var Ряд = 0; Ряд < ЧислоРядов; Ряд++) {
            const ЭтотРяд = КодыНаКлавиатуре[Ряд];
            const ЧислоКнопокВРяду = ЭтотРяд.length;
            for (var НомКнопВРяду = 0; НомКнопВРяду < ЧислоКнопокВРяду; НомКнопВРяду++) {
                const Изо = ИзображениеКнопки(Ряд, НомКнопВРяду);
                const ТекстЭтойКнопки = Раскладка2[Ряд][НомКнопВРяду][ключРаскладки];
                Изо.html(ТекстЭтойКнопки);
            }
        }
    }

    // https://stackoverflow.com/questions/10073699/pad-a-number-with-leading-zeros-in-javascript   
    function ДополниНулямиСлева(n, width, z) {
      z = z || '0';
      n = n + '';
      return n.length >= width ? n : new Array(width - n.length + 1).join(z) + n;
}

    // ЧтоНапечатается - либо буква, которую хотим напечатать по нажатию на некую кнопку, либо строка "<НЕТ>". 
    // Выдаёт то, что можно вставить в исходный текст программы, а именно, код буквы в виде U<ПослеU>HHHH>
    // Вместо <НЕТ> выдаёт код звёздочки "*"
    function Заюникодь(ЧтоНапечатается,ПослеU) {
      if (ПослеU === undefined) {
        ПослеU = "";
      }
      if (ЧтоНапечатается === undefined) {
         console.assert("Нечего напечатать");
         ЧтоНапечатается = "!";
      } else if (ЧтоНапечатается === "<НЕТ>") {
         ЧтоНапечатается = "*";
      } else if (ЧтоНапечатается.length > 1) {
         console.assert("Не знаю, как получить код строки '%s'", ЧтоНапечатается);
         ЧтоНапечатается = "!";
      }
      const УниКод = ЧтоНапечатается.charCodeAt(0);
      console.assert(УниКод <= 65535, "Попалась буква %s со слишком большим кодом %d", ЧтоНапечатается, УниКод);
      return "U"+ПослеU+ДополниНулямиСлева(УниКод.toString(16),4,0);
    }
    
    /// Генерирует фрагмент кода для яро-раскладки на базе AutoHotkey
    function ГенерируйКодДляAutoHotkeyВнутр() {
    
        /// Закавычивание одной буквы по правилам клавиатурных событий
        /// AutoHotkey - для других целей не годится. Функция не используется,
        /// сохранена на всякий случай
        function Закавычь(Эс) {  
            Эс = Эс.replace(/\"/,"\\\"");
            if(Эс == '"'){return '\\"'}else if(Эс == "{"){return "{{}"
            }else if(Эс == "}"){return "{}}"}else{return Эс}}
        
        var Рез = ";;; Сгенерировано с помощью ГенерируйКодДляAutoHotkey() из веб-браузер/F12\n";
        Рез = Рез + ";;; https://bitbucket.org/budden/iaroklava-js\n";
        const ЧислоРядов = КодыНаКлавиатуре.length;
        for (var Ряд = 0; Ряд < ЧислоРядов; Ряд++) {
            const ЭтотРяд = КодыНаКлавиатуре[Ряд];
            const ЧислоКнопокВРяду = ЭтотРяд.length;
            for (var НомКнопВРяду = 0; НомКнопВРяду < ЧислоКнопокВРяду; НомКнопВРяду++) {
                /// vk = virtual key code
                const vkЭтойКнопки = ЭтотРяд[НомКнопВРяду];
                const БуквыДляКнопки = Раскладка2[Ряд][НомКнопВРяду];
                console.assert(!(БуквыДляКнопки === undefined));
                const vk16 = vkЭтойКнопки.toString(16);
                const Лямбда = function(ПризнакРегистраAutoHotkey,ИндексРаскладки) {
                    const ПР = ПризнакРегистраAutoHotkey;
                    const ЧтоНапечатается = БуквыДляКнопки[ИндексРаскладки];
                    const НапечатаетсяКод = Заюникодь(ЧтоНапечатается,"+");
                    Рез = Рез + "$" + ПР + "vk" + vk16 +
                        ":: Нажато(\""+ ПР +"{vk" + vk16 + "}\",\"{" +
                        НапечатаетсяКод + "}\") ; "+ ЧтоНапечатается +"\n";
                }
                if (vkЭтойКнопки != 32) {
                    Рез = Рез + ";; " + БуквыДляКнопки[0] + "\n";
                    Лямбда("",2); // просто кнопка (AltGr)
                    Лямбда("+",3); // AltGr + Shift
                }
            }
        } 
        Рез = Рез + ";;; Конец кода, сгенерированного ГенерируйКодДляAutoHotkey()\n";
        console.log(Рез);
    }
    
    // Фрагмент для /usr/share/X11/xkb/symbols/ru 
    function ГенерируйКодДляФайлаSymbolsВнутр() {
        const Отступ = "    ";
        var Рез = Отступ + "/// Сгенерировано с помощью ГенерируйКодДляФайлаSymbolsВнутр() из веб-браузер/F12\n";
        Рез = Рез + Отступ + "/// https://bitbucket.org/budden/iaroklava-js\n";
        const ЧислоРядов = Значения_key_для_symbols.length;
        for (var Ряд = 0; Ряд < ЧислоРядов; Ряд++) {
            const ЭтотРяд = Значения_key_для_symbols[Ряд];
            const ЧислоКнопокВРяду = ЭтотРяд.length;
            for (var НомКнопВРяду = 0; НомКнопВРяду < ЧислоКнопокВРяду; НомКнопВРяду++) {
                const БуквыДляКнопки = Раскладка2[Ряд][НомКнопВРяду];
                console.assert(!(БуквыДляКнопки === undefined));
                const Б0 = БуквыДляКнопки[0]; const Т0 = Заюникодь(Б0,"");
                const Б1 = БуквыДляКнопки[1]; const Т1 = Заюникодь(Б1,"");
                const Б2 = БуквыДляКнопки[2]; const Т2 = Заюникодь(Б2,"");
                const Б3 = БуквыДляКнопки[3]; const Т3 = Заюникодь(Б3,"");
                /// k = keycode (man xmodmap)
                const kЭтойКнопки = ЭтотРяд[НомКнопВРяду];
                if (kЭтойКнопки != "SPCE") { 
                    Рез = Рез + Отступ + "key <" + kЭтойКнопки + "> { [ "
                    + Т0 + ", " + Т1 + ", " + Т2 + ", " + Т3 
                    + " ] }; // " 
                    + Б0 + " " + Б1 + " " + Б2 + " " + Б3 + "\n";
                }
            }
        }
        Рез = Рез + Отступ + "/// Конец кода, сгенерированного ГенерируйКодДляФайлаSymbolsВнутр()\n";
        console.log(Рез);
    }

    ГенерируйКодДляФайлаSymbols = ГенерируйКодДляФайлаSymbolsВнутр;
    ГенерируйКодДляAutoHotkey = ГенерируйКодДляAutoHotkeyВнутр;

    //// Строго говоря, мы не знаем состояние кнопок в момент, когда мы сюда попали.
    /// РежимПробела
    // 0 - отжат
    // 1 - нажат и пока не было другой буквы
    // 2 - нажат и уже была другая буква
    var РежимПробела = 0;

    /// Режим Shift - true, если нажат. CAPS LOCK мы игнорируем (извините).
    var СостояниеШифта = false;

    $.fn.Instant_translit = function (settings) {
        settings = jQuery.extend({
                event_now: "",
                obect_now: ""
            },
            settings);

        /*ПРАВЬМЯ КРОСС-БРАУЗЕРНАЯ ПЕРЕНОСИМОСТЬ - http://unixpapa.com/js/key.html
           Яро-раскладка https://ic.pics.livejournal.com/budden73/16984381/8764/8764_original.png
           ПРАВЬМЯ в Яро-раскладке = - это dead key */
        get_translate(settings.event_now, settings.obect_now);
    };

    /**
     * Function for translate, all web browser but not IE
     * evnt object events
     * раскладки  array  кодТекущейРаскладки
     * obect object
     */
    function get_translate(evnt, obect) {
        var code = evnt.keyCode ? evnt.keyCode : void 0;
        //var txt = "";
        if (!evnt.which) {
            return true;
        }
        if (!!evnt.ctrlKey || !!evnt.altKey) {
            return true;
        }

        var txt = code;
        КодКлавиши = txt;
        $("#СтрСост_event_тчк_type").text("evnt.type: " + evnt.type);
        $("#СтрСостПримечание").text("keyCode: " + code);

        // Есть проблема с auto-repeat, см., напр, http://unixpapa.com/js/key.html - может возникать keyup.
        // если это случится, то единственный (кривоватый) выход - это смотреть на время. Т.е. после keyup
        // нужно прождать какое-то небольшое время. Если keydown не придёт, то значит, что клавиша отжата.
        // и в зависимости от этого уже нужно интерпретировать другие события - их тоже придётся на некоторое время
        // забуферизовать. setTimeout, к примеру. Альтернативный вариант - смотреть по событиям и выдать пользователю
        // подозрение, что в его системе кривой autorepeat. Тогда это хотя бы будет не на нашей совести.
        if (evnt.type === "keydown") {
            if (code === КодКлавишиShift) {
                ЗаметьНажатияОсобыхКнопок(РежимПробела, true);
            } else if (code === КодКлавишиПробел) {
                if (РежимПробела === 0) {
                    ЗаметьНажатияОсобыхКнопок(1, evnt.shiftKey);
                } else if (!!evnt.repeat) {
                    /// пробел зажат, события продолжают лететь. Пусть их.
                } else {
                    /// это что-то странное, но не будем ничего делать - авось прокатит
                    /// В Хром под Windows repeat не пришёл... Поэтому ничего не делаем
                    //console.log("Снова нажимается пробел без autorepeat");
                }
                if (evnt.preventDefault) evnt.preventDefault();
                return false; // keydown для пробела никогда не приводит к появлению пробела в тексте.
            }
        }

        else if (evnt.type === "keyup") {
            if (code === КодКлавишиShift) {
                ЗаметьНажатияОсобыхКнопок(РежимПробела, false);
            } else if (code === КодКлавишиПробел) {
                if (РежимПробела === 1) {
                    /// это - отжатый пробел, долгий или короткий. В случае, если autorepeat выдаёт keyup, мы сюда будем приходить при autorepeat
                    /// Снимаем РежимПробела, и затем пробел попадёт в текст.
                    ЗаметьНажатияОсобыхКнопок(0, evnt.shiftKey);
                } else if (РежимПробела === 2) {
                    /// пробел поработал модификатором. Литера пробела не нужна в тексте
                    ЗаметьНажатияОсобыхКнопок(0, evnt.shiftKey);
                    if (evnt.preventDefault) evnt.preventDefault();
                    return false;
                } else {
                    /// А кстати, что, если мы нажали пробел и поставили фокус в это место?
                    console.log("Пробел отжат, хотя мы не заметили, чтобы он был нажат");
                }
            } else {
                /// А значит, это keyup, но не пробел. Поглощаем его, т.к. уже приняли keydown
                if (evnt.preventDefault) evnt.preventDefault();
                return false;
            }
        }
        if (code !== КодКлавишиПробел && РежимПробела === 1) {
            /// Пробел работает модификатором - не приведёт к появлению кнопки "пробел" в тексте
            ЗаметьНажатияОсобыхКнопок(2, evnt.shiftKey);
        }

        const ключРаскладки = ДайКлючРаскладки(evnt.shiftKey)

        const ЧислоРядов = КодыНаКлавиатуре.length;
        var ОбрабатываемЭтуКнопку = false;
        var Ряд = 0;
        var НомКнопВРяду = -1;
        for (; Ряд < ЧислоРядов; Ряд++) {
            НомКнопВРяду = ГдеЖеЭлементВМассиве(КодыНаКлавиатуре[Ряд], КодКлавиши);
            if (НомКнопВРяду >= 0) {
                ОбрабатываемЭтуКнопку = true;
                setTimeout(function () {
                    ПодсветиКнопку(Ряд, НомКнопВРяду), 0
                }, 5);
                break;
            }
        }

        $("#СтрСост_ключРаскладки").text("ключРаскладки: " + Ряд + " " + НомКнопВРяду);
        if (ОбрабатываемЭтуКнопку) {
            const ТекстЭтойКнопки = Раскладка2[Ряд][НомКнопВРяду][ключРаскладки];
            if ((typeof ТекстЭтойКнопки) !== "string") {
                alert("Не найдена раскладка для кнопки: Ряд " + Ряд + ", НомКнопВРяду:" + НомКнопВРяду + ", ключРаскладки:" + ключРаскладки + " typeof ТекстЭтойКнопки:" + (typeof ТекстЭтойКнопки));
                return false;
            }
            if ((typeof obect.value === "undefined") || (obect.value.length === 0)) {
                obect.value = ТекстЭтойКнопки;
                if (evnt.preventDefault) evnt.preventDefault();
                return false;
            }
            const current_position = getCaretPos(obect);

            const pretxt = obect.value.substring(0, current_position);
            const therest = obect.value.substr(current_position);

            obect.value = pretxt + ТекстЭтойКнопки + therest;
            goTo(current_position + ТекстЭтойКнопки.length, obect);
            if (evnt.preventDefault) evnt.preventDefault();
            return false;
        }
        //return false;
    }

    /// shiftKey - из event.shiftKey. Также используем РежимПробела из глобального состояния
    function ДайКлючРаскладки() {
        const битыРежимаShift = СостояниеШифта ? 1 : 0;
        // ПРАВЬМЯ ДУБЛИРОВАНИЕ
        const битыРежимаПробела = (РежимПробела !== 0) ? 2 : 0;
        return +битыРежимаShift + битыРежимаПробела;
    }

    function ЗаметьНажатияОсобыхКнопок(НовыйРежимПробела, НовоеСостояниеШифта) {
        НовоеСостояниеШифта = !!НовоеСостояниеШифта;

        const Перерисовать = (РежимПробела !== НовыйРежимПробела)
            || (СостояниеШифта !== НовоеСостояниеШифта);

        РежимПробела = НовыйРежимПробела;
        СостояниеШифта = НовоеСостояниеШифта;

        if (Перерисовать) {
            const КлючРаскладки = ДайКлючРаскладки();
            setTimeout(function () {
                ПокажиРаскладку(КлючРаскладки);
            }, 5);
        }
        setTimeout(function () {
            $("#СтрСост_РежимПробела").text("РежимПробела: " + РежимПробела);
        }, 3);
    }

    /**
     * Function for determining the position of the pointer
     * obj  object
     */
    function getCaretPos(obj) {
        // Мы пытаемся избавиться от лишних событий, т.к. часть ввода с клавиатуры мы пропускаем в textarea без
        // всякой обработки
        // obj.focus();
        const Рез = $.caretPos(obj);
        return Рез * 1;
        // return getInputSelection(obj).start;
    }


    /// https://stackoverflow.com/users/96100/tim-down
    /// https://stackoverflow.com/questions/263743/how-to-get-the-caret-position-in-a-textarea-in-characters-from-the-start/3373056#3373056

    /**
     * Function to move the pointer
     * n  int position you want to move the pointer
     * o  object
     */
    function goTo(n, o) {
        console.assert(typeof o.id === "string");
        const jqueryId = "#" + o.id;
        const oInJquery = $(jqueryId);
        oInJquery.caret(n);
    }

    function СкажиПользователюЧтобыНеЖалНаКнопку(ОбъектСобытие) {
        alert("Пожалуйста, печатайте на клавиатуре вашего компьютера. Данная страничка - это не экранная клавиатура, а демонстрация способа ввода двузязычных текстов без переключения раскладки");
    }

    $(".key").click(СкажиПользователюЧтобыНеЖалНаКнопку);
    $("#textarea_для_ввода").focus();

    /// Выбо
    function ПокажиИлиСкройЭтуКлавиатуру() {
        $("#рискноп_раскладка").toggle();
    }

    $("#рискноп_покажи_или_скрой_раскладку").click(ПокажиИлиСкройЭтуКлавиатуру)

    /// На входе дан div клавиатуры, нужно его привязать и показать то, что надо.
    function ПодготовьЭтуКлавиатуруКРаботе(div) {
        console.assert(div.hasAttribute("data-target"));
        const id_of_textarea = div.getAttribute("data-target");
        //div.innerHTML="<p>для ввода латиницы удерживай пробел <button id='рискноп_показать_раскладку' title='Показать раскладку'>?</button></p>";
        const Выборка = $("textarea#"+id_of_textarea);
        console.assert(Выборка.length === 1, "Не к чему привязать клавиатуру - нет text_area с id = '%s'", id_of_textarea);
        УстановиОбработчикиКлавиатуры(Выборка);
        /// На сайте программирование-по-русски приходится оборачивать клавиатуру в скрытый во время загрузки div,
        /// поскольку тормозит блок 'поделиться'. В примере в репозитории этот код ничего не делает ввиду пустоты выборок.
        $("#СодержимоеДляПоказаВместоКлавиатурыВоВремяЗагрузки").ё().hide();
        $("#СодержимоеКлавиатурыПоказатьПослеЗагрузки").ё().show();
        /// Установка фокуса также нужна на сайте, т.к. окно ввода сначала скрыто
        $("#textarea_для_ввода").focus();
    }

    // это делать при показе клавы
    ЗаметьНажатияОсобыхКнопок(0);
    ПодготовьЭтуКлавиатуруКРаботе($("#рискноп")[0])
}); // $(document).ready(function ($) {

